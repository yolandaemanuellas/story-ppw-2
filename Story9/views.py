from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def home(request):
    return render(request, 'home.html')

def sign_up(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            #add session
            request.session['name'] = user.username
            return redirect('Story9:home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form, 'errors': form.errors})
	  
def log_in(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            #add session
            request.session['name'] = user.username
            return redirect('Story9:home')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form, 'errors': form.errors})

def log_out(request):
    if request.method == 'POST':
        logout(request)
        #delete session
        request.session.flush()
        return redirect('Story9:login')

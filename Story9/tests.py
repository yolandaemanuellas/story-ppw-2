from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your tests here.
class HomeTest(TestCase):

    def test_if_url_home_exists(self):
        response = Client().get('/Story9/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_signup_exists(self):
        response = Client().get('/Story9/signup/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_login_exists(self):
        response = Client().get('/Story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_if_url_wrong(self):
        response = Client().get('/test/')
        self.assertEqual(response.status_code, 404)

    def test_sign_up_succeed(self):
        Client().post('/Story9/signup/', data = {"username":"dynamite", "password1":"lifegoeson", "password2":"lifegoeson"})
        self.assertEqual(User.objects.all().count(), 1)

    def test_sign_up_failed(self):
        Client().post('/Story9/signup/', data = {"username":"dynamite", "password1":"loveyourself", "password2":"homerun"})
        self.assertEqual(User.objects.all().count(), 0)

    def test_log_in(self):
        Client().post('/Story9/signup/', data = {"username":"dynamite", "password1":"lifegoeson", "password2":"lifegoeson"})
        Client().post('/Story9/login/', data = {"username":"dynamite", "password":"lifegoeson"})

    def test_log_out(self):
        Client().post('/Story9/logout/',data = {"username":"dynamite", "password":"lifegoeson"})

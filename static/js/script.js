(function ($) {
	$(".accordion > li:eq(0) a").addClass("active").next().slideDown();

	$(".accordion a").click(function (j) {
		var dropDown = $(this).closest("li").find("p");

		$(this).closest(".accordion").find("p").not(dropDown).slideUp();

		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
    	} else {
      		$(this).closest(".accordion").find("a.active").removeClass("active");
      		$(this).addClass("active");
    	}

    	dropDown.stop(false, true).slideToggle();

    	j.preventDefault();
    	
  	});

  	$('.up').click(function() {
		var item = $(this).parents('div.set'),
			swapWith = item.prev();
		item.after(swapWith);
	});

  	$('.down').click(function() {
		var item = $(this).parents('div.set'),
			swapWith = item.next();
		item.before(swapWith);
	});

})(jQuery);

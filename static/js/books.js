$(document).ready(() => {
    var url_called = 'http://yolanda-emanuellas-2.herokuapp.com/Story8/data?q=' + 'korean'
    $.ajax({
        method: 'GET',
        url: url_called,
        success: function(response) { 
            var object_result = $("#result");
            object_result.empty();

            var table = '<div class="table-responsive-sm"><table class="table table-striped table-bordered">';
            table += '<thead class="thead-dark"><tr><th scope="col">No</th><th scope="col">Book Name</th><th scope="col">Author</th><th scope="col">Thumbnail</th></tr></thead><tbody>';

            for(i=0; i<response.items.length; i++) {
                var thumbnail = 'https://www.eric-forum.eu/wp-content/uploads/2019/04/no-thumbnail.jpg';
                var title = response.items[i].volumeInfo.title;
                var authors = response.items[i].volumeInfo.authors;
                if (!(response.items[i].volumeInfo.imageLinks === undefined)) {
                    thumbnail = response.items[i].volumeInfo.imageLinks.smallThumbnail;
                }

                table += '<tr><th scope="row">' + (i+1) + '</th><td>' + title + '</td><td>' + authors + '</td><td>' + '<img src="' + thumbnail + '" alt="picture" />' + '</td>';
            }

            table += '</tr></tbody></table></div>'
            object_result.append(table);
        }
    });

    $("#search").keyup( function() {
        var keyword = $("#search").val();
        var url_called_2 = 'http://yolanda-emanuellas-2.herokuapp.com/Story8/data?q=' + keyword
        $.ajax({
            method: 'GET',
            url: url_called_2,
            success: function(response) { 
                console.log('response')
                console.log(response)

                var object_result = $("#result");
                object_result.empty(); 

                var table = '<div class="table-responsive-sm"><table class="table table-striped table-bordered">';
                table += '<thead class="thead-dark"><tr><th scope="col">No</th><th scope="col">Book Name</th><th scope="col">Author</th><th scope="col">Thumbnail</th></tr></thead><tbody>';

                for(i=0; i<response.items.length; i++) {
                    var thumbnail = 'https://www.eric-forum.eu/wp-content/uploads/2019/04/no-thumbnail.jpg';
                    var title = response.items[i].volumeInfo.title;
                    var authors = response.items[i].volumeInfo.authors;
                    if (!(response.items[i].volumeInfo.imageLinks === undefined)) {
                        thumbnail = response.items[i].volumeInfo.imageLinks.smallThumbnail;
                    }

                    table += '<tr><th scope="row">' + (i+1) + '</th><td>' + title + '</td><td>' + authors + '</td><td>' + '<img src="' + thumbnail + '" alt="picture" />' + '</td>';
                }

                table += '</tr></tbody></table></div>'
                object_result.append(table);
            }
        });
    });
})

from django.test import TestCase, Client

# Create your tests here.
class PageTest(TestCase):

    def test_if_url_exists(self):
        response = Client().get('/Story7/')
        self.assertEqual(response.status_code,200)

    def test_if_url_wrong(self):
        response = Client().get('/test/')
        self.assertEqual(response.status_code,404)

    def test_if_content_exists(self):
        response = Client().get('/Story7/')
        content = response.content.decode('utf8')
        self.assertIn('Hello!', content)
        self.assertIn('Welcome to my profile :)', content)
        self.assertIn('Aktivitas', content)
        self.assertIn('Pengalaman Organisasi/Kepanitiaan', content)
        self.assertIn('Prestasi', content)
        self.assertIn('Riwayat Pendidikan', content)
        self.assertIn('Up', content)
        self.assertIn('Down', content)

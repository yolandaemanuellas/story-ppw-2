from . import views
from django.urls import path

app_name = 'Story7'

urlpatterns = [
	path('', views.page, name='page'),
]
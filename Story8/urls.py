from . import views
from django.urls import path

app_name = 'Story8'

urlpatterns = [
	path('', views.index, name='index'),
	path('data/', views.data, name='data'),
]
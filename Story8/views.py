from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def index(request):
	return render(request,'index.html')

def data(request):
	q = request.GET['q']
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + q
    
	response = requests.get(url)
	response_json = response.json()
	return JsonResponse(response_json)

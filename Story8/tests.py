from django.test import TestCase, Client

# Create your tests here.
class IndexTest(TestCase):

    def test_if_url_exists(self):
        response = Client().get('/Story8/')
        self.assertEqual(response.status_code,200)

    def test_if_url_wrong(self):
        response = Client().get('/test/')
        self.assertEqual(response.status_code,404)

    def test_if_template_used(self):
        Client().get('/Story8/')
        self.assertTemplateUsed('Story8/index.html')

    def test_if_json_is_returned(self):
        response = Client().get('/Story8/data/?q=korean')
        self.assertEqual(response.status_code,200)

    def test_if_content_exists(self):
        response = Client().get('/Story8/')
        content = response.content.decode('utf8')
        self.assertIn("I'm searching for this book:", content)
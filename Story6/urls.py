from . import views
from django.urls import path

app_name = 'story6'

urlpatterns = [
	path('', views.activity_list, name='activity_list'),
	path('add-activity/', views.add_activity, name='add_activity'),
	path('add-participants/<int:activity_id>', views.add_participant, name='add_participant'),
]

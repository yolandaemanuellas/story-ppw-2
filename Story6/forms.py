from django import forms
from .models import Activity, Participant

class ActivityForm(forms.ModelForm):
	class Meta:
		model = Activity
		fields = [
			'activity_name'
		]

		labels = {
			'activity_name': 'Nama kegiatan'
		}

		widgets = {
			'activity_name': forms.TextInput(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan nama kegiatan...'
				}
			)
		}

class ParticipantForm(forms.ModelForm):
	class Meta:
		model = Participant
		fields = [
			'participant_name'
		]

		labels = {
			'participant_name': 'Nama peserta'
		}

		widgets = {
			'participant_name': forms.TextInput(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan nama peserta...'
				}
			)
		}
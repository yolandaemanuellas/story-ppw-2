from django.test import TestCase, Client
from .models import Activity, Participant
from .forms import ActivityForm, ParticipantForm

# Create your tests here.
class ActivityModelTest(TestCase):

	def test_string_representation(self):
		activity = Activity(activity_name='Nobar')
		self.assertEqual(str(activity), activity.activity_name)

	def test_verbose_name_plural(self):
		self.assertEqual(str(Activity._meta.verbose_name_plural), "Activities")

class ParticipantModelTest(TestCase):

	def test_string_representation(self):
		activity = Activity(activity_name='Nobar')
		participant = Participant(participant_name='Yola', activity=activity)
		self.assertEqual(str(participant), participant.participant_name + " - " + participant.activity.activity_name)

	def test_verbose_name_plural(self):
		self.assertEqual(str(Participant._meta.verbose_name_plural), "Participants")

class Story6Test(TestCase):

	def test_if_activity_list_page_return_activity_objects(self):
		activity = Activity.objects.create(
			activity_name='Mabar'
		)

		participant = Participant.objects.create(
			participant_name = 'Yola',
			activity = activity
		)

		response = Client().get('/Story6/')
		content = response.content.decode('utf8')
		self.assertIn(activity.activity_name, content)
		self.assertIn(participant.participant_name, content)

	def test_if_activity_form_page_return_form(self):
		response = Client().get('/Story6/add-activity/')
		content = response.content.decode('utf8')
		self.assertIn('Tambah Kegiatan', content)

	def test_activity_form_post(self):
		response = Client().post('/Story6/add-activity/', data={"activity_name": "Mabar lagi kuy"})
		self.assertEqual(Activity.objects.all().count(),1)

	def test_if_participant_form_page_return_form(self):
		activity = Activity.objects.create(
			activity_name='Nobar deh yuk'
		)

		response = Client().get('/Story6/add-participants/'+str(activity.id))
		content = response.content.decode('utf8')
		self.assertIn('Tambah Peserta', content)

	def test_participant_form_post(self):
		activity = Activity.objects.create(
			activity_name='Nobar deh yuk'
		)

		response = Client().post('/Story6/add-participants/'+str(activity.id), data={"participant_name": "Yola"})
		self.assertEqual(activity.participant_set.all().count(),1)
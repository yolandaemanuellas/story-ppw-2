from django.shortcuts import render, redirect
from .forms import ActivityForm, ParticipantForm
from .models import Activity, Participant

# Create your views here.
def activity_list(request):
	activities = Activity.objects.all()
	activity_list = []
	for activity in activities:
		participants_in_activity = activity.participant_set.all()
		activity_list.append({
			'activity': activity,
			'participants': participants_in_activity 
		})
	response = {
		'title': 'Daftar Kegiatan',
		'activity_list': activity_list
	}

	return render(request, 'activity_list.html', response)

def add_activity(request):
	if (request.method == "POST"):
		activity_form = ActivityForm(request.POST)
		if activity_form.is_valid():
			activity_form.save()
		return redirect('story6:activity_list')
	else:
		activity_form = ActivityForm()
		response = {
			'title': 'Tambah Kegiatan',
			'activity_form': activity_form
		}
		return render(request, 'activity_form.html', response)

def add_participant(request, activity_id):
	if (request.method == "POST"):
		participant_form = ParticipantForm(request.POST)
		if participant_form.is_valid():
			activity = Activity.objects.get(id=activity_id)
			participant = participant_form.save(commit=False)
			participant.activity = activity
			participant.save()
			return redirect('story6:activity_list')
	else:
		participant_form = ParticipantForm()
		response = {
			'title': 'Tambah Peserta',
			'participant_form': participant_form,
			'activity': Activity.objects.get(id=activity_id),
		}
		return render(request, 'participant_form.html', response)
from django.db import models

# Create your models here.
class Activity(models.Model):

	class Meta:
		verbose_name_plural = 'Activities'

	activity_name = models.CharField(max_length=255)

	def __str__(self):
		return self.activity_name

class Participant(models.Model):

	class Meta:
		verbose_name_plural = 'Participants'

	participant_name = models.CharField(max_length=255)
	activity = models.ForeignKey(Activity, on_delete=models.CASCADE)

	def __str__(self):
		return (self.participant_name + " - " + self.activity.activity_name)